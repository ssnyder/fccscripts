EvtMax = globals().get('EvtMax', 500)
InputFile = globals().get('InputFile', 'tops_edm4hep.root')
OutputFile = globals().get('OutputFile', 'tops_cld.root')

from Gaudi.Configuration import *

from Configurables import LcioEvent, k4DataSvc, MarlinProcessorWrapper
from k4MarlinWrapper.parseConstants import *
from k4MarlinWrapper.MarlinProcessorWrapper import \
 InitializeDD4hep, DDPlanarDigiProcessor, TruthTrackFinder, AIDAProcessor, \
 ConformalTrackingV2, ClonesAndSplitTracksFinder, ClicEfficiencyCalculator, \
 TrackChecker, Statusmonitor, RefitFinal, DDSimpleMuonDigi, RecoMCTruthLinker, \
 HitResiduals, MarlinLumiCalClusterer, MergeCollections, FastJetProcessor, \
 LcfiplusProcessor, LCIOOutputProcessor, CLICPfoSelector, Args
algList = []


CONSTANTS = {
             'CalorimeterIntegrationTimeWindow': "10",
}

parseConstants(CONSTANTS)


# For converters
from Configurables import ToolSvc, Lcio2EDM4hepTool, EDM4hep2LcioTool

from Configurables import k4DataSvc, PodioInput
evtsvc = k4DataSvc('EventDataSvc')
evtsvc.input = InputFile

inp = PodioInput('InputReader')
inp.collections = [
  'EventHeader',
  'MCParticles',
  'VertexBarrelCollection',
  'VertexEndcapCollection',
  'InnerTrackerBarrelCollection',
  'OuterTrackerBarrelCollection',
  'ECalEndcapCollection',
  'ECalEndcapCollectionContributions',
  'ECalBarrelCollection',
  'ECalBarrelCollectionContributions',
  'HCalBarrelCollection',
  'HCalBarrelCollectionContributions',
  'InnerTrackerEndcapCollection',
  'OuterTrackerEndcapCollection',
  'HCalEndcapCollection',
  'HCalEndcapCollectionContributions',
  'HCalRingCollection',
  'HCalRingCollectionContributions',
  'YokeBarrelCollection',
  'YokeBarrelCollectionContributions',
  'YokeEndcapCollection',
  'YokeEndcapCollectionContributions',
  'LumiCalCollection',
  'LumiCalCollectionContributions',
]
inp.OutputLevel = WARNING


# EDM4hep to LCIO converter
edmConvTool = EDM4hep2LcioTool("EDM4hep2lcio")
edmConvTool.convertAll = False
edmConvTool.collNameMapping = {
    'MCParticles':                     'MCParticles',
    'VertexBarrelCollection':          'VertexBarrelCollection',
    'VertexEndcapCollection':          'VertexEndcapCollection',
    'InnerTrackerBarrelCollection':    'InnerTrackerBarrelCollection',
    'OuterTrackerBarrelCollection':    'OuterTrackerBarrelCollection',
    'InnerTrackerEndcapCollection':    'InnerTrackerEndcapCollection',
    'OuterTrackerEndcapCollection':    'OuterTrackerEndcapCollection',
    'ECalEndcapCollection':            'ECalEndcapCollection',
    'ECalBarrelCollection':            'ECalBarrelCollection',
    'HCalBarrelCollection':            'HCalBarrelCollection',
    'HCalEndcapCollection':            'HCalEndcapCollection',
    'HCalRingCollection':              'HCalRingCollection',
    'YokeBarrelCollection':            'YokeBarrelCollection',
    'YokeEndcapCollection':            'YokeEndcapCollection',
    'LumiCalCollection':               'LumiCalCollection',
}
edmConvTool.OutputLevel = DEBUG



MyAIDAProcessor = AIDAProcessor \
    ('MyAIDAProcessor',
     OutputLevel = WARNING,
     Compress = 1,
     FileName = 'histograms',
     FileType = 'root',
     EDM4hep2LcioTool = edmConvTool)


InitDD4hep = InitializeDD4hep \
    ("InitDD4hep",
     OutputLevel = WARNING,
     # Changed version to v05 to match sim...
     DD4hepXMLFile =  os.environ["LCGEO"]+"/FCCee/compact/FCCee_o1_v05/FCCee_o1_v05.xml",
     EncodingStringParameter = "GlobalTrackerReadoutID")

VXDBarrelDigitiser = DDPlanarDigiProcessor \
    ('VXDBarrelDigitiser',
     OutputLevel = WARNING,
     IsStrip = False,
     ResolutionU = [0.003, 0.003, 0.003, 0.003, 0.003, 0.003],
     ResolutionV = [0.003, 0.003, 0.003, 0.003, 0.003, 0.003],
     SimTrackHitCollectionName = 'VertexBarrelCollection',
     SimTrkHitRelCollection    = 'VXDTrackerHitRelations',
     SubDetectorName           = 'Vertex',
     TrackerHitCollectionName  = 'VXDTrackerHits')

VXDEndcapDigitiser = DDPlanarDigiProcessor \
    ('VXDEndcapDigitiser',
     OutputLevel = WARNING,
     IsStrip = False,
     ResolutionU = [0.003, 0.003, 0.003, 0.003, 0.003, 0.003],
     ResolutionV = [0.003, 0.003, 0.003, 0.003, 0.003, 0.003],
     SimTrackHitCollectionName = 'VertexEndcapCollection',
     SimTrkHitRelCollection    = 'VXDEndcapTrackerHitRelations',
     SubDetectorName           = 'Vertex',
     TrackerHitCollectionName  = 'VXDEndcapTrackerHits')

InnerPlanarDigiProcessor = DDPlanarDigiProcessor \
    ('InnerPlanarDigiProcessor',
     OutputLevel = WARNING,
     IsStrip = False,
     ResolutionU = [0.007],
     ResolutionV = [0.09],
     SimTrackHitCollectionName = 'InnerTrackerBarrelCollection',
     SimTrkHitRelCollection    = 'InnerTrackerBarrelHitsRelations',
     SubDetectorName           = 'InnerTrackers',
     TrackerHitCollectionName  = 'ITrackerHits')

InnerEndcapPlanarDigiProcessor = DDPlanarDigiProcessor \
    ('InnerEndcapPlanarDigiProcessor',
     OutputLevel = WARNING,
     IsStrip = False,
     ResolutionU = [0.005, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007],
     ResolutionV = [0.005, 0.09,  0.09,  0.09,  0.09,  0.09,  0.09],
     SimTrackHitCollectionName = 'InnerTrackerEndcapCollection',
     SimTrkHitRelCollection    = 'InnerTrackerEndcapHitsRelations',
     SubDetectorName           = 'InnerTrackers',
     TrackerHitCollectionName  = 'ITrackerEndcapHits')

OuterPlanarDigiProcessor = DDPlanarDigiProcessor \
    ('OuterPlanarDigiProcessor',
     OutputLevel = WARNING,
     IsStrip = False,
     ResolutionU = [0.007, 0.007, 0.007],
     ResolutionV = [0.09,  0.09,  0.09],
     SimTrackHitCollectionName = 'OuterTrackerBarrelCollection',
     SimTrkHitRelCollection    = 'OuterTrackerBarrelHitsRelations',
     SubDetectorName           = 'OuterTrackers',
     TrackerHitCollectionName  = 'OTrackerHits')

OuterEndcapPlanarDigiProcessor = DDPlanarDigiProcessor \
    ('OuterEndcapPlanarDigiProcessor',
     OutputLevel = WARNING,
     IsStrip = False,
     ResolutionU = [0.007, 0.007, 0.007, 0.007, 0.007],
     ResolutionV = [0.09,  0.09,  0.09,  0.09,  0.09],
     SimTrackHitCollectionName = 'OuterTrackerEndcapCollection',
     SimTrkHitRelCollection    = 'OuterTrackerEndcapHitsRelations',
     SubDetectorName           = 'OuterTrackers',
     TrackerHitCollectionName  = 'OTrackerEndcapHits')


MyTruthTrackFinder = TruthTrackFinder \
    ('MyTruthTrackFinder',
     OutputLevel = WARNING,
     FitForward = True,
     MCParticleCollectionName = 'MCParticles',
     SiTrackCollectionName = 'SiTracks',
     SiTrackRelationCollectionName = 'SiTrackRelations',
     SimTrackerHitRelCollectionNames = ['VXDTrackerHitRelations',
                                        'InnerTrackerBarrelHitsRelations',
                                        'OuterTrackerBarrelHitsRelations',
                                        'VXDEndcapTrackerHitRelations',
                                        'InnerTrackerEndcapHitsRelations',
                                        'OuterTrackerEndcapHitsRelations'],
     TrackerHitCollectionNames = ['VXDTrackerHits',
                                  'ITrackerHits',
                                  'OTrackerHits',
                                  'VXDEndcapTrackerHits',
                                  'ITrackerEndcapHits',
                                  'OTrackerEndcapHits'],
     UseTruthInPrefit = False)

MyConformalTracking = ConformalTrackingV2 \
    ('MyConformalTracking',
     OutputLevel = WARNING,
     DebugHits = 'DebugHits',
     DebugPlots = False,
     DebugTiming = False,
     MCParticleCollectionName = 'MCParticles',
     MaxHitInvertedFit = 0,
     MinClustersOnTrackAfterFit = 3,
     RelationsNames = ['VXDTrackerHitRelations',
                       'VXDEndcapTrackerHitRelations',
                       'InnerTrackerBarrelHitsRelations',
                       'OuterTrackerBarrelHitsRelations',
                       'InnerTrackerEndcapHitsRelations',
                       'OuterTrackerEndcapHitsRelations'],
     RetryTooManyTracks = False,
     SiTrackCollectionName ='SiTracksCT',
     SortTreeResults = True,
#     Steps = ["[VXDBarrel]", "@Collections", ":", "VXDTrackerHits", "@Parameters", ":", "MaxCellAngle", ":", "0.01;", "MaxCellAngleRZ", ":", "0.01;", "Chi2Cut", ":", "100;", "MinClustersOnTrack", ":", "4;", "MaxDistance", ":", "0.03;", "SlopeZRange:", "10.0;", "HighPTCut:", "10.0;", "@Flags", ":", "HighPTFit,", "VertexToTracker", "@Functions", ":", "CombineCollections,", "BuildNewTracks", "[VXDEncap]", "@Collections", ":", "VXDEndcapTrackerHits", "@Parameters", ":", "MaxCellAngle", ":", "0.01;", "MaxCellAngleRZ", ":", "0.01;", "Chi2Cut", ":", "100;", "MinClustersOnTrack", ":", "4;", "MaxDistance", ":", "0.03;", "SlopeZRange:", "10.0;", "HighPTCut:", "10.0;", "@Flags", ":", "HighPTFit,", "VertexToTracker", "@Functions", ":", "CombineCollections,", "ExtendTracks", "[LowerCellAngle1]", "@Collections", ":", "VXDTrackerHits,", "VXDEndcapTrackerHits", "@Parameters", ":", "MaxCellAngle", ":", "0.05;", "MaxCellAngleRZ", ":", "0.05;", "Chi2Cut", ":", "100;", "MinClustersOnTrack", ":", "4;", "MaxDistance", ":", "0.03;", "SlopeZRange:", "10.0;HighPTCut:", "10.0;", "@Flags", ":", "HighPTFit,", "VertexToTracker,", "RadialSearch", "@Functions", ":", "CombineCollections,", "BuildNewTracks", "[LowerCellAngle2]", "@Collections", ":", "@Parameters", ":", "MaxCellAngle", ":", "0.1;", "MaxCellAngleRZ", ":", "0.1;", "Chi2Cut", ":", "2000;", "MinClustersOnTrack", ":", "4;", "MaxDistance", ":", "0.03;", "SlopeZRange:", "10.0;", "HighPTCut:", "10.0;", "@Flags", ":", "HighPTFit,", "VertexToTracker,", "RadialSearch", "@Functions", ":", "BuildNewTracks,", "SortTracks", "[Tracker]", "@Collections", ":", "ITrackerHits,", "OTrackerHits,", "ITrackerEndcapHits,", "OTrackerEndcapHits", "@Parameters", ":", "MaxCellAngle", ":", "0.1;", "MaxCellAngleRZ", ":", "0.1;", "Chi2Cut", ":", "2000;", "MinClustersOnTrack", ":", "4;", "MaxDistance", ":", "0.03;", "SlopeZRange:", "10.0;", "HighPTCut:", "1.0;", "@Flags", ":", "HighPTFit,", "VertexToTracker,", "RadialSearch", "@Functions", ":", "CombineCollections,", "ExtendTracks", "[Displaced]", "@Collections", ":", "VXDTrackerHits,", "VXDEndcapTrackerHits,", "ITrackerHits,", "OTrackerHits,", "ITrackerEndcapHits,", "OTrackerEndcapHits", "@Parameters", ":", "MaxCellAngle", ":", "0.1;", "MaxCellAngleRZ", ":", "0.1;", "Chi2Cut", ":", "1000;", "MinClustersOnTrack", ":", "5;", "MaxDistance", ":", "0.015;", "SlopeZRange:", "10.0;", "HighPTCut:", "10.0;", "@Flags", ":", "OnlyZSchi2cut,", "RadialSearch", "@Functions", ":", "CombineCollections,", "BuildNewTracks"],
     Steps = """[VXDBarrel]
@Collections: VXDTrackerHits
@Parameters:
  MaxCellAngle       = 0.01;
  MaxCellAngleRZ     = 0.01;
  Chi2Cut            = 100;
  MinClustersOnTrack = 4;
  MaxDistance        = 0.03;
  SlopeZRange        = 10.0;
  HighPTCut          = 10.0;
@Flags: HighPTFit, VertexToTracker
@Functions: CombineCollections, BuildNewTracks

[VXDEndcap]
@Collections: VXDEndcapTrackerHits
@Parameters:
  MaxCellAngle       = 0.01;
  MaxCellAngleRZ     = 0.01;
  Chi2Cut            = 100;
  MinClustersOnTrack = 4;
  MaxDistance        = 0.03;
  SlopeZRange        = 10.0;
  HighPTCut          = 10.0;
@Flags: HighPTFit, VertexToTracker
@Functions: CombineCollections, ExtendTracks

[LowerCellAngle1]
@Collections: VXDTrackerHits, VXDEndcapTrackerHits
@Parameters:
  MaxCellAngle       = 0.05;
  MaxCellAngleRZ     = 0.05;
  Chi2Cut            = 100;
  MinClustersOnTrack = 4;
  MaxDistance        = 0.03;
  SlopeZRange        = 10.0;
  HighPTCut          = 10.0;
@Flags: HighPTFit, VertexToTracker, RadialSearch
@Functions: CombineCollections, BuildNewTracks

[LowerCellAngle2]
@Collections:
@Parameters:
  MaxCellAngle       = 0.1;
  MaxCellAngleRZ     = 0.1;
  Chi2Cut            = 2000;
  MinClustersOnTrack = 4;
  MaxDistance        = 0.03;
  SlopeZRange        = 10.0;
  HighPTCut          = 10.0;
@Flags: HighPTFit, VertexToTracker, RadialSearch
@Functions: BuildNewTracks, SortTracks

[Tracker]
@Collections: ITrackerHits, OTrackerHits, ITrackerEndcapHits, OTrackerEndcapHits
@Parameters:
  MaxCellAngle       = 0.1;
  MaxCellAngleRZ     = 0.1;
  Chi2Cut            = 2000;
  MinClustersOnTrack = 4;
  MaxDistance        = 0.03;
  SlopeZRange        = 10.0;
  HighPTCut          = 1.0;
@Flags: HighPTFit, VertexToTracker, RadialSearch
@Functions: CombineCollections, ExtendTracks

[Displaced]
@Collections: VXDTrackerHits, VXDEndcapTrackerHits, ITrackerHits, OTrackerHits, ITrackerEndcapHits, OTrackerEndcapHits
@Parameters:
  MaxCellAngle       = 0.1;
  MaxCellAngleRZ     = 0.1;
  Chi2Cut            = 1000;
  MinClustersOnTrack = 5;
  MaxDistance        = 0.015;
  SlopeZRange        = 10.0;
  HighPTCut          = 10.0;
@Flags: OnlyZSchi2cut, RadialSearch
@Functions: CombineCollections, BuildNewTracks
""".replace('\n', ' '),
     ThetaRange = 0.05,
     TooManyTracks = 100000,
     TrackerHitCollectionNames = ['VXDTrackerHits',
                                  'VXDEndcapTrackerHits',
                                  'ITrackerHits',
                                  'OTrackerHits',
                                  'ITrackerEndcapHits',
                                  'OTrackerEndcapHits'],
     trackPurity = 0.7)

ClonesAndSplitTracksFinder = ClonesAndSplitTracksFinder \
    ('ClonesAndSplitTracksFinder',
     OutputLevel = WARNING,
     EnergyLossOn = True,
     InputTrackCollectionName = 'SiTracksCT',
     MultipleScatteringOn = True,
     OutputTrackCollectionName = 'SiTracks',
     SmoothOn = False,
     extrapolateForward = True,
     maxSignificancePhi= 3,
     maxSignificancePt = 2,
     maxSignificanceTheta = 3,
     mergeSplitTracks = False,
     minTrackPt = 1)

Refit = RefitFinal \
    ('Refit',
     OutputLevel = WARNING,
     EnergyLossOn = True,
     InputRelationCollectionName = 'SiTrackRelations',
     InputTrackCollectionName = 'SiTracks',
     Max_Chi2_Incr = 1.79769e+30,
     MinClustersOnTrackAfterFit = 3,
     MultipleScatteringOn = True,
     OutputRelationCollectionName = 'SiTracks_Refitted_Relation',
     OutputTrackCollectionName = 'SiTracks_Refitted',
     ReferencePoint = -1,
     SmoothOn = False,
     extrapolateForward = True)

MyClicEfficiencyCalculator = ClicEfficiencyCalculator \
    ('MyClicEfficiencyCalculator',
     OutputLevel = WARNING,
     MCParticleCollectionName = 'MCParticles',
     MCParticleNotReco = 'MCParticleNotReco',
     MCPhysicsParticleCollectionName = 'MCPhysicsParticles',
     TrackCollectionName = 'SiTracks_Refitted',
     TrackerHitCollectionNames = ['VXDTrackerHits',
                                  'VXDEndcapTrackerHits',
                                  'ITrackerHits',
                                  'OTrackerHits',
                                  'ITrackerEndcapHits',
                                  'OTrackerEndcapHits'],
     TrackerHitRelCollectionNames = ['VXDTrackerHitRelations',
                                     'VXDEndcapTrackerHitRelations',
                                     'InnerTrackerBarrelHitsRelations',
                                     'OuterTrackerBarrelHitsRelations',
                                     'InnerTrackerEndcapHitsRelations',
                                     'OuterTrackerEndcapHitsRelations'],
     efficiencyTreeName = 'trktree',
     mcTreeName = 'mctree',
     morePlots = False,
     purityTreeName = 'puritytree',
     reconstructableDefinition = 'ILDLike',
     vertexBarrelID = 1)

MyTrackChecker = TrackChecker \
    ('MyTrackChecker',
     OutputLevel = WARNING,
     MCParticleCollectionName = 'MCParticles',
     TrackCollectionName = 'SiTracks_Refitted',
     TrackRelationCollectionName = 'SiTracksMCTruthLink',
     TreeName = 'checktree',
     UseOnlyTree = True)

EventNumber = Statusmonitor \
    ('EventNumber',
     OutputLevel = WARNING,
     HowOften = 1)

MyDDSimpleMuonDigi = DDSimpleMuonDigi \
    ('MyDDSimpleMuonDigi',
     OutputLevel = WARNING,
     CalibrMUON = 70.1,
     MUONCollections = ['YokeBarrelCollection',
                        'YokeEndcapCollection'],
     MUONOutputCollection = 'MUON',
     MaxHitEnergyMUON = '2.0',
     MuonThreshold = 1e-06,
     RelationOutputCollection = 'RelationMuonHit')

MyStatusmonitor = Statusmonitor \
    ('MyStatusmonitor',
     OutputLevel = WARNING,
     HowOften = 100)

MyRecoMCTruthLinker = RecoMCTruthLinker \
    ('MyRecoMCTruthLinker',
     OutputLevel = WARNING,
     BremsstrahlungEnergyCut = 1,
     CalohitMCTruthLinkName = 'CalohitMCTruthLink',
     ClusterCollection = 'PandoraClusters',
     ClusterMCTruthLinkName = 'ClusterMCTruthLink',
     FullRecoRelation = False,
     InvertedNonDestructiveInteractionLogic = False,
     KeepDaughtersPDG = [22, 111, 310, 13, 211, 321, 3120],
     MCParticleCollection = 'MCPhysicsParticles',
     MCParticlesSkimmedName = 'MCParticlesSkimmed',
     MCTruthClusterLinkName = '',
     MCTruthRecoLinkName = '',
     MCTruthTrackLinkName = '',
     RecoMCTruthLinkName = 'RecoMCTruthLink',
     RecoParticleCollection = 'PandoraPFOs',
     SaveBremsstrahlungPhotons = False,
     SimCaloHitCollections = ['ECalBarrelCollection',
                              'ECalEndcapCollection',
                              'HCalBarrelCollection',
                              'HCalEndcapCollection',
                              'HCalRingCollection',
                              'YokeBarrelCollection',
                              'YokeEndcapCollection',
                              'LumiCalCollection'],
     SimCalorimeterHitRelationNames = ['RelationCaloHit', 'RelationMuonHit'],
     SimTrackerHitCollections = ['VertexBarrelCollection',
                                 'VertexEndcapCollection',
                                 'InnerTrackerBarrelCollection',
                                 'OuterTrackerBarrelCollection',
                                 'InnerTrackerEndcapCollection',
                                 'OuterTrackerEndcapCollection'],
     TrackCollection = 'SiTracks_Refitted',
     TrackMCTruthLinkName = 'SiTracksMCTruthLink',
     TrackerHitsRelInputCollections = ['VXDTrackerHitRelations',
                                       'VXDEndcapTrackerHitRelations',
                                       'InnerTrackerBarrelHitsRelations',
                                       'OuterTrackerBarrelHitsRelations',
                                       'InnerTrackerEndcapHitsRelations',
                                       'OuterTrackerEndcapHitsRelations'],
     UseTrackerHitRelations = True,
     UsingParticleGun = False,
     daughtersECutMeV= 10)

MyHitResiduals = HitResiduals \
    ('MyHitResiduals',
     OutputLevel = WARNING,
     EnergyLossOn = True,
     MaxChi2Increment = 1000,
     MultipleScatteringOn = True,
     SmoothOn = False,
     TrackCollectionName= 'SiTracks_Refitted',
     outFileName = 'residuals.root',
     treeName = 'restree')

LumiCalReco = MarlinLumiCalClusterer \
    ('LumiCalReco',
     OutputLevel = WARNING,
     ClusterMinNumHits = 15,
     ElementsPercentInShowerPeakLayer = 0.03,
     EnergyCalibConst= 0.01213,
     LogWeigthConstant = 6.5,
     LumiCal_Clusters = 'LumiCalClusters',
     LumiCal_Collection = 'LumiCalCollection',
     LumiCal_RecoParticles =  'LumiCalRecoParticles',
     MaxRecordNumber = 10,
     MemoryResidentTree = 0,
     MiddleEnergyHitBoundFrac = 0.01,
     MinClusterEngy = 2.0,
     MinHitEnergy = 20e-06,
     MoliereRadius = 20,
     NumEventsTree = 500,
     NumOfNearNeighbor = 6,
     OutDirName = 'rootOut',
     OutRootFileName = '',
     SkipNEvents = 0,
     WeightingMethod = 'LogMethod',
     ZLayerPhiOffset = 0.0)

RenameCollection = MergeCollections \
    ('RenameCollection',
     OutputLevel = WARNING,
     CollectionParameterIndex = 0,
     InputCollectionIDs = [],
     InputCollections = ['PandoraPFOs'],
     OutputCollection = 'PFOsFromJets')

MyFastJetProcessor = FastJetProcessor \
    ('MyFastJetProcessor',
     OutputLevel = WARNING,
     algorithm = ['ValenciaPlugin', 1.2, 1.0, 0.7],
     clusteringMode = ['ExclusiveNJets', 2],
     jetOut = 'JetsAfterGamGamRemoval',
     recParticleIn = 'TightSelectedPandoraPFOs',
     recParticleOut = 'PFOsFromJets',
     recombinationScheme = 'E_scheme',
     storeParticlesInJets = True)


def makeJetClustering():
    from k4MarlinWrapper.MarlinProcessorWrapper import Args
    return Args (name = 'JetClustering',
                 AlphaParameter = 1.0,
                 BetaParameter = 1.0,
                 GammaParameter = 1.0,
                 InputVertexCollectionName = 'BuildUpVertices',
                 JetAlgorithm = 'ValenciaVertex',
                 MaxNumberOfJetsForYThreshold = 10,
                 MuonIDExternal = 0,
                 MuonIDMaximum3DImpactParameter = 5.0,
                 MuonIDMinimumD0Significance = 5.0,
                 MuonIDMinimumEnergy = 0,
                 MuonIDMinimumProbability = 0.5,
                 MuonIDMinimumZ0Significance = 5.0,
                 NJetsRequested = 2,
                 OutputJetCollectionName = 'VertexJets',
                 OutputJetStoresVertex = 0,
                 PrimaryVertexCollectionName = 'PrimaryVertices',
                 RParameter = 1.0,
                 UseBeamJets = 1,
                 UseMuonID = 1,
                 VertexSelectionK0MassWidth = 0.02,
                 VertexSelectionMaximumDistance = 30.,
                 VertexSelectionMinimumDistance = 0.3,
                 YAddedForJetLeptonLepton = 100,
                 YAddedForJetLeptonVertex = 100,
                 YAddedForJetVertexLepton = 0,
                 YAddedForJetVertexVertex = 100,
                 YCut = 0.)


def makeJetVertexRefiner():
    from k4MarlinWrapper.MarlinProcessorWrapper import Args
    return Args (name = 'JetVertexRefiner',
                 BNessCut = -0.80,
                 BNessCutE1 = -0.15,
                 InputJetCollectionName = 'VertexJets',
                 InputVertexCollectionName = 'BuildUpVertices',
                 MaxAngleSingle = 0.5,
                 MaxCharmFlightLengthPerJetEnergy = 0.1,
                 MaxPosSingle = 30.,
                 MaxSeparationPerPosSingle = 0.1,
                 MinEnergySingle = 1.,
                 MinPosSingle = 0.3,
                 OneVertexProbThreshold = 0.001,
                 OutputJetCollectionName = 'RefinedVertexJets',
                 OutputVertexCollectionName = 'RefinedVertices',
                 PrimaryVertexCollectionName = 'PrimaryVertices',
                 V0VertexCollectionName = 'BuildUpVertices_V0',
                 mind0sigSingle = 5.,
                 minz0sigSingle = 5.,
                 useBNess = 0)


def makeLcfiplusProcessor (name, Algorithms = [], **kw):
    from k4MarlinWrapper.MarlinProcessorWrapper import LcfiplusProcessor
    defs = dict(OutputLevel = WARNING,
                MCPCollection = 'MCParticles',
                MCPFORelation = 'RecoMCTruthLink',
                MagneticField = 2.0,
                PFOCollection = 'PFOsFromJets',
                PrintEventNumber = 1,
                ReadSubdetectorEnergies = 0,
                TrackHitOrdering = 2,
                UpdateVertexRPDaughters = 0,
                UseMCP = 0,
                Algorithms = [a.name for a in Algorithms])
    defs.update (kw)
    for a in Algorithms:
        defs[a.name] = a
        del a.name
    return LcfiplusProcessor (name, **defs)


JetClusteringAndRefiner = makeLcfiplusProcessor \
    ('JetClusteringAndRefiner',
     Algorithms = [makeJetClustering(), makeJetVertexRefiner()])


Output_REC = LCIOOutputProcessor \
    ('Output_REC',
     OutputLevel = WARNING,
     DropCollectionNames = [],
     DropCollectionTypes = [],
     FullSubsetCollections = ['EfficientMCParticles', 'InefficientMCParticles'],
     KeepCollectionNames =  [],
     LCIOOutputFile = 'Output_REC.slcio"',
     LCIOWriteMode = 'WRITE_NEW')


# LCIO to EDM4hep converter
lcioConvTool = Lcio2EDM4hepTool("lcio2EDM4hep")
lcioConvTool.OutputLevel = DEBUG
lcioConvTool.convertAll = False
lcioConvTool.collNameMapping = {
    'BuildUpVertices' : 'BuildUpVertices',
    'BuildUpVertices_RP' : 'BuildUpVertices_RP',
    'ParticleID_EXT' : 'ParticleID_EXT',
    'Vertex_EXT' : 'Vertex_EXT',
    'BuildUpVertices_V0' : 'BuildUpVertices_V0',
    'BuildUpVertices_V0_0' : 'BuildUpVertices_V0_0',
    'BuildUpVertices_V0_RP' : 'BuildUpVertices_V0_RP',
    'CalohitMCTruthLink' : 'CalohitMCTruthLink',
    'DebugHits' : 'DebugHits',
    'ECALBarrel' : 'ECALBarrel',
    'ECALEndcap' : 'ECALEndcap',
    'EfficientMCParticles' : 'EfficientMCParticles',
    'HCALBarrel' : 'HCALBarrel',
    'HCALEndcap' : 'HCALEndcap',
    'HCALOther' : 'HCALOther',
    'ITrackerEndcapHits' : 'ITrackerEndcapHits',
    'ITrackerHits' : 'ITrackerHits',
    'InefficientMCParticles' : 'InefficientMCParticles',
    'LooseSelectedPandoraPFOs' : 'LooseSelectedPandoraPFOs',
    'MCParticlesSkimmed' : 'MCParticlesSkimmed',
    'MCPhysicsParticles' : 'MCPhysicsParticles',
    'MUON' : 'MUON',
    'OTrackerEndcapHits' : 'OTrackerEndcapHits',
    'OTrackerHits' : 'OTrackerHits',
    'PFOsFromJets' : 'PFOsFromJets',
    'PandoraClusters' : 'PandoraClusters',
    'PandoraPFOs' : 'PandoraPFOs',
    'PandoraStartVertices' : 'PandoraStartVertices',
    'PrimaryVertices' : 'PrimaryVertices',
    'PrimaryVertices_0' : 'PrimaryVertices_0',
    'PrimaryVertices_RP' : 'PrimaryVertices_RP',
    'RecoMCTruthLink' : 'RecoMCTruthLink',
    'RefinedVertexJets' : 'RefinedVertexJets',
    'RefinedVertexJets_rel' : 'RefinedVertexJets_rel',
    'RefinedVertexJets_vtx' : 'RefinedVertexJets_vtx',
    'RefinedVertexJets_vtx_0' : 'RefinedVertexJets_vtx_0',
    'RefinedVertexJets_vtx_RP' : 'RefinedVertexJets_vtx_RP',
    'RefinedVertices' : 'RefinedVertices',
    'RefinedVertices_0' : 'RefinedVertices_0',
    'RefinedVertices_RP' : 'RefinedVertices_RP',
    'RelationCaloHit' : 'RelationCaloHit',
    'RelationMuonHit' : 'RelationMuonHit',
    'SelectedPandoraPFOs' : 'SelectedPandoraPFOs',
    'SiTracks' : 'SiTracks',
    'SiTracks_0' : 'SiTracks_0',
    'SiTracks_1' : 'SiTracks_1',
    'SiTracks_2' : 'SiTracks_2',
    'SiTracksCT' : 'SiTracksCT',
    'SiTracksCT_0' : 'SiTracksCT_0',
    'SiTracksCT_1' : 'SiTracksCT_1',
    'SiTracksCT_2' : 'SiTracksCT_2',
    'SiTracksMCTruthLink' : 'SiTracksMCTruthLink',
    'SiTracks_Refitted' : 'SiTracks_Refitted',
    'SiTracks_Refitted_0' : 'SiTracks_Refitted_0',
    'SiTracks_Refitted_1' : 'SiTracks_Refitted_1',
    'SiTracks_Refitted_2' : 'SiTracks_Refitted_2',
    'TightSelectedPandoraPFOs' : 'TightSelectedPandoraPFOs',
    'VXDEndcapTrackerHits' : 'VXDEndcapTrackerHits',
    'VXDTrackerHits' : 'VXDTrackerHits',
    'VertexJets' : 'VertexJets',
}
Output_REC.Lcio2EDM4hepTool=lcioConvTool


Output_DST = LCIOOutputProcessor \
    ('Output_DST',
     OutputLevel = WARNING,
     DropCollectionNames = [],
     DropCollectionTypes = ['MCParticles', 'LCRelation', 'SimCalorimeterHit', 'CalorimeterHit', 'SimTrackerHit', 'TrackerHit', 'TrackerHitPlane', 'Track', 'ReconstructedParticle', 'LCFloatVec'],
     FullSubsetCollections = ['EfficientMCParticles', 'InefficientMCParticles', 'MCPhysicsParticles'],
     KeepCollectionNames = ['MCParticlesSkimmed', 'MCPhysicsParticles', 'RecoMCTruthLink', 'SiTracks', 'SiTracks_Refitted', 'PandoraClusters', 'PandoraPFOs', 'SelectedPandoraPFOs', 'LooseSelectedPandoraPFOs', 'TightSelectedPandoraPFOs', 'RefinedVertexJets', 'RefinedVertexJets_rel', 'RefinedVertexJets_vtx', 'RefinedVertexJets_vtx_RP', 'BuildUpVertices', 'BuildUpVertices_res', 'BuildUpVertices_RP', 'BuildUpVertices_res_RP', 'BuildUpVertices_V0', 'BuildUpVertices_V0_res', 'BuildUpVertices_V0_RP', 'BuildUpVertices_V0_res_RP', 'PrimaryVertices', 'PrimaryVertices_res', 'PrimaryVertices_RP', 'PrimaryVertices_res_RP', 'RefinedVertices', 'RefinedVertices_RP'],
     LCIOOutputFile = 'Output_DST.slcio',
     LCIOWriteMode = 'WRITE_NEW')

def makeOverlayTimingGeneric (name, **kw):
    from k4MarlinWrapper.MarlinProcessorWrapper import OverlayTimingGeneric
    defs = dict (OutputLevel = WARNING,
                 BackgroundFileNames = [],
                 Collection_IntegrationTimes = ['VertexBarrelCollection', 380, 'VertexEndcapCollection', 380, 'InnerTrackerBarrelCollection', 380, 'InnerTrackerEndcapCollection', 380, 'OuterTrackerBarrelCollection', 380, 'OuterTrackerEndcapCollection', 380, 'ECalBarrelCollection', 380, 'ECalEndcapCollection', 380, 'HCalBarrelCollection', 380, 'HCalEndcapCollection', 380, 'HCalRingCollection', 380, 'YokeBarrelCollection', 380, 'YokeEndcapCollection', 380, 'LumiCalCollection', 380],
                 Delta_t = 20,
                 MCParticleCollectionName = 'MCParticles',
                 MCPhysicsParticleCollectionName = 'MCPhysicsParticles',
                 NBunchtrain = 0,
                 NumberBackground = 0.,
                 PhysicsBX = 1,
                 Poisson_random_NOverlay = False,
                 RandomBx = False,
                 # Shift back start time to allow for vertex smearing.
                 Start_Integration_Time = -10.0,
                 TPCDriftvelocity = 0.05)
    defs.update (kw)
    return OverlayTimingGeneric (name, **defs)
              

OverlayFalse = makeOverlayTimingGeneric ('OverlayFalse')


Overlay91GeV = makeOverlayTimingGeneric \
    ('Overlay91GeV',
     BackgroundFileNames = ['pairs_Z_sim.slcio'],
     NBunchtrain = 20,
     NumberBackground = 1.)


Overlay365GeV = makeOverlayTimingGeneric \
    ('Overlay365GeV',
     BackgroundFileNames = ['pairs_Z_sim.slcio'],
     Delta_t = 3396,
     NBunchtrain = 3,
     NumberBackground = 1.)


MyDDCaloDigi = {}

def makeDDCaloDigi (name, **kw):
    from k4MarlinWrapper.MarlinProcessorWrapper import DDCaloDigi
    d = dict (OutputLevel = WARNING,
              CalibECALMIP = 0.0001,
              CalibHCALMIP = 0.0001,
              CalibrECAL = [37.5227197175, 37.5227197175],
              CalibrHCALBarrel = 45.9956826061,
              CalibrHCALEndcap = 46.9252540291,
              CalibrHCALOther = 57.4588011802,
              ECALBarrelTimeWindowMax = 10,
              ECALCollections = ['ECalBarrelCollection', 'ECalEndcapCollection'],
              ECALCorrectTimesForPropagation = 1,
              ECALDeltaTimeHitResolution = 10,
              ECALEndcapCorrectionFactor = 1.03245503522,
              ECALEndcapTimeWindowMax = 10,
              ECALGapCorrection = 1,
              ECALGapCorrectionFactor = 1,
              ECALLayers =  [41, 100],
              ECALModuleGapCorrectionFactor = 0.0,
              ECALOutputCollection0 = 'ECALBarrel',
              ECALOutputCollection1 = 'ECALEndcap',
              ECALOutputCollection2 = 'ECALOther',
              ECALSimpleTimingCut = True,
              ECALThreshold = 5e-05,
              ECALThresholdUnit = 'GeV',
              ECALTimeResolution = 10,
              # Changed from -1 to -10 to allow for vtx smearing
              ECALTimeWindowMin = -10,
              ECAL_PPD_N_Pixels = 10000,
              ECAL_PPD_N_Pixels_uncertainty = 0.05,
              ECAL_PPD_PE_per_MIP = 7,
              ECAL_apply_realistic_digi = 0,
              ECAL_deadCellRate = 0,
              ECAL_deadCell_memorise = False,
              ECAL_default_layerConfig = '000000000000000',
              ECAL_elec_noise_mips = 0,
              ECAL_maxDynamicRange_MIP = 2500,
              ECAL_miscalibration_correl = 0,
              ECAL_miscalibration_uncorrel = 0,
              ECAL_miscalibration_uncorrel_memorise = False,
              ECAL_pixel_spread = 0.05,
              ECAL_strip_absorbtionLength = 1e+06,
              HCALBarrelTimeWindowMax = 10,
              HCALCollections = ['HCalBarrelCollection', 'HCalEndcapCollection', 'HCalRingCollection'],
              HCALCorrectTimesForPropagation = 1,
              HCALDeltaTimeHitResolution = 10,
              HCALEndcapCorrectionFactor = 1.000,
              HCALEndcapTimeWindowMax = 10,
              HCALGapCorrection = 1,
              HCALLayers = [100],
              HCALModuleGapCorrectionFactor = 0.5,
              HCALOutputCollection0 = 'HCALBarrel',
              HCALOutputCollection1 = 'HCALEndcap',
              HCALOutputCollection2 = 'HCALOther',
              HCALSimpleTimingCut = True,
              HCALThreshold = 0.00025,
              HCALThresholdUnit = 'GeV',
              HCALTimeResolution = 10,
              # Changed from -1 to -10 to allow for vtx smearing
              HCALTimeWindowMin = -10,
              HCAL_PPD_N_Pixels = 400,
              HCAL_PPD_N_Pixels_uncertainty = 0.05,
              HCAL_PPD_PE_per_MIP = 10,
              HCAL_apply_realistic_digi = 0,
              HCAL_deadCellRate = 0,
              HCAL_deadCell_memorise = False,
              HCAL_elec_noise_mips = 0,
              HCAL_maxDynamicRange_MIP = 200,
              HCAL_miscalibration_correl = 0,
              HCAL_miscalibration_uncorrel = 0,
              HCAL_miscalibration_uncorrel_memorise = False,
              HCAL_pixel_spread = 0,
              Histograms = 0,
              IfDigitalEcal = 0,
              IfDigitalHcal = 0,
              MapsEcalCorrection = 0,
              RelationOutputCollection = 'RelationCaloHit',
              RootFile = 'Digi_SiW.root',
              StripEcal_default_nVirtualCells = 9,
              UseEcalTiming = 1,
              UseHcalTiming = 1,
              energyPerEHpair = 3.6)
    d.update (kw)
    return DDCaloDigi (name, **d)

    
MyDDCaloDigi["10"] = makeDDCaloDigi ('MyDDCaloDigi_10ns')

MyDDCaloDigi["400"] = makeDDCaloDigi \
    ('MyDDCaloDigi_400ns',
     CalibrECAL = ['37.4591745147', '37.4591745147'],
     CalibrHCALBarrel = 42.544403752,
     CalibrHCALEndcap = 42.9667604345,
     CalibrHCALOther = 51.3503963688,
     ECALBarrelTimeWindowMax = 400,
     ECALEndcapCorrectionFactor = 1.01463983425,
     ECALEndcapTimeWindowMax = 400,
     HCALBarrelTimeWindowMax = 400,
     HCALEndcapTimeWindowMax = 400)


def makeDDPandoraPFANewProcessor (name, caloIntegrationWindow, **kw):
    from k4MarlinWrapper.MarlinProcessorWrapper import DDPandoraPFANewProcessor
    wdep = {}
    wdep[10] = dict \
        (ECalToEMGeVCalibration = 1.01776966108,
         ECalToHadGeVCalibrationBarrel = 1.11490774181,
         ECalToHadGeVCalibrationEndCap = 1.11490774181,
         ECalToMipCalibration = 175.439,
         HCalToEMGeVCalibration = 1.01776966108,
         HCalToHadGeVCalibration = 1.00565042407,
         HCalToMipCalibration = 45.6621,
         PandoraSettingsXmlFile = 'PandoraSettingsFCCee/PandoraSettingsDefault.xml',
         SoftwareCompensationWeights = [2.40821, -0.0515852, 0.000711414, -0.0254891, -0.0121505, -1.63084e-05, 0.062149, 0.0690735, -0.223064])

    wdep[400] = dict \
        (ECalToEMGeVCalibration = 1.02513816926,
         ECalToHadGeVCalibrationBarrel = 1.07276660331,
         ECalToHadGeVCalibrationEndCap = 1.07276660331,
         ECalToMipCalibration = 175.439,
         HCalToEMGeVCalibration = 1.02513816926,
         HCalToHadGeVCalibration = 1.01147686143,
         HCalToMipCalibration = 49.7512,
         PandoraSettingsXmlFile = 'PandoraSettingsFCCee/PandoraSettingsDefault_400nsCalTimeWindow.xml',
         SoftwareCompensationWeights = [2.43375, -0.0430951, 0.000244914, -0.145478, -0.00044577, -8.37222e-05, 0.237484, 0.243491, -0.0713701])

    defs = dict (OutputLevel = WARNING,
                 ClusterCollectionName = 'PandoraClusters',
                 CreateGaps = False,
                 CurvatureToMomentumFactor = 0.00015,
                 D0TrackCut = 200,
                 D0UnmatchedVertexTrackCut = 5,
                 DigitalMuonHits = 0,
                 ECalBarrelNormalVector = [0, 0, 1],
                 ECalCaloHitCollections = ['ECALBarrel', 'ECALEndcap', 'ECALOther'],
                 ECalMipThreshold = 0.5,
                 ECalScMipThreshold = 0,
                 ECalScToEMGeVCalibration = 1,
                 ECalScToHadGeVCalibrationBarrel = 1,
                 ECalScToHadGeVCalibrationEndCap = 1,
                 ECalScToMipCalibration = 1,
                 ECalSiMipThreshold = 0,
                 ECalSiToEMGeVCalibration = 1,
                 ECalSiToHadGeVCalibrationBarrel = 1,
                 ECalSiToHadGeVCalibrationEndCap = 1,
                 ECalSiToMipCalibration = 1,
                 EMConstantTerm = 0.01,
                 EMStochasticTerm = 0.17,
                 FinalEnergyDensityBin = 110.,
                 HCalBarrelNormalVector = [0, 0, 1],
                 HCalCaloHitCollections = ['HCALBarrel', 'HCALEndcap', 'HCALOther'],
                 HCalMipThreshold = 0.3,
                 HadConstantTerm = 0.03,
                 HadStochasticTerm = 0.6,
                 InputEnergyCorrectionPoints = [],
                 KinkVertexCollections = ['KinkVertices'],
                 LCalCaloHitCollections = [],
                 LHCalCaloHitCollections = [],
                 LayersFromEdgeMaxRearDistance = 250,
                 MCParticleCollections = ['MCParticles'],
                 MaxBarrelTrackerInnerRDistance = 200,
                 MaxClusterEnergyToApplySoftComp = 200.,
                 MaxHCalHitHadronicEnergy = 10000000.,
                 MaxTrackHits = 5000,
                 MaxTrackSigmaPOverP = 0.15,
                 MinBarrelTrackerHitFractionOfExpected = 0,
                 MinCleanCorrectedHitEnergy = 0.1,
                 MinCleanHitEnergy = 0.5,
                 MinCleanHitEnergyFraction = 0.01,
                 MinFtdHitsForBarrelTrackerHitFraction = 0,
                 MinFtdTrackHits = 0,
                 MinMomentumForTrackHitChecks = 0,
                 MinTpcHitFractionOfExpected = 0,
                 MinTrackECalDistanceFromIp = 0,
                 MinTrackHits = 0,
                 MuonBarrelBField = -1.0,
                 MuonCaloHitCollections = ['MUON'],
                 MuonEndCapBField = 0.01,
                 MuonHitEnergy = 0.5,
                 MuonToMipCalibration = 20703.9,
                 NEventsToSkip = 0,
                 NOuterSamplingLayers = 3,
                 OutputEnergyCorrectionPoints = [],
                 PFOCollectionName = 'PandoraPFOs',
                 ProngVertexCollections = 'ProngVertices',
                 ReachesECalBarrelTrackerOuterDistance = -100,
                 ReachesECalBarrelTrackerZMaxDistance = -50,
                 ReachesECalFtdZMaxDistance = 1,
                 ReachesECalMinFtdLayer = 0,
                 ReachesECalNBarrelTrackerHits = 0,
                 ReachesECalNFtdHits = 0,
                 RelCaloHitCollections = ['RelationCaloHit', 'RelationMuonHit'],
                 RelTrackCollections = ['SiTracks_Refitted_Relation'],
                 ShouldFormTrackRelationships = 1,
                 SplitVertexCollections = ['SplitVertices'],
                 StartVertexAlgorithmName = 'PandoraPFANew',
                 StartVertexCollectionName = 'PandoraStartVertices',
                 StripSplittingOn = 0,
                 TrackCollections  = ['SiTracks_Refitted'],
                 TrackCreatorName = 'DDTrackCreatorCLIC',
                 TrackStateTolerance = 0,
                 TrackSystemName = 'DDKalTest',
                 UnmatchedVertexTrackMaxEnergy = 5,
                 UseEcalScLayers = 0,
                 UseNonVertexTracks = 1,
                 UseOldTrackStateCalculation = 0,
                 UseUnmatchedNonVertexTracks = 0,
                 UseUnmatchedVertexTracks = 1,
                 V0VertexCollections = ['V0Vertices'],
                 YokeBarrelNormalVector = [0, 0, 1],
                 Z0TrackCut = 200,
                 Z0UnmatchedVertexTrackCut = 5,
                 ZCutForNonVertexTracks = 250
                 )
    defs.update (wdep[caloIntegrationWindow])
    defs.update (kw)
    return DDPandoraPFANewProcessor (name, **defs)


MyDDMarlinPandora = makeDDPandoraPFANewProcessor("MyDDMarlinPandora_10ns",
                                                 int(CONSTANTS["CalorimeterIntegrationTimeWindow"]))


MyCLICPfoSelectorDefault = CLICPfoSelector \
    ('MyCLICPfoSelectorDefault',
     OutputLevel = WARNING,
     ChargedPfoLooseTimingCut = 3,
     ChargedPfoNegativeLooseTimingCut = -1,
     ChargedPfoNegativeTightTimingCut = -0.5,
     ChargedPfoPtCut = 0,
     ChargedPfoPtCutForLooseTiming = 4,
     ChargedPfoTightTimingCut = 1.5,
     CheckKaonCorrection = 0,
     CheckProtonCorrection = 0,
     ClusterLessPfoTrackTimeCut = 10,
     CorrectHitTimesForTimeOfFlight = 0,
     DisplayRejectedPfos = 1,
     DisplaySelectedPfos = 1,
     FarForwardCosTheta = 0.975,
     ForwardCosThetaForHighEnergyNeutralHadrons = 0.95,
     ForwardHighEnergyNeutralHadronsEnergy = 10,
     HCalBarrelLooseTimingCut = 20,
     HCalBarrelTightTimingCut = 10,
     HCalEndCapTimingFactor = 1,
     InputPfoCollection = 'PandoraPFOs',
     KeepKShorts = 1,
     MaxMomentumForClusterLessPfos = 2,
     MinECalHitsForTiming = 5,
     MinHCalEndCapHitsForTiming = 5,
     MinMomentumForClusterLessPfos = 0.5,
     MinPtForClusterLessPfos = 0.5,
     MinimumEnergyForNeutronTiming = 1,
     Monitoring = 0,
     MonitoringPfoEnergyToDisplay = 1,
     NeutralFarForwardLooseTimingCut = 2,
     NeutralFarForwardTightTimingCut = 1,
     NeutralHadronBarrelPtCutForLooseTiming = 3.5,
     NeutralHadronLooseTimingCut = 2.5,
     NeutralHadronPtCut = 0,
     NeutralHadronPtCutForLooseTiming = 8,
     NeutralHadronTightTimingCut = 1.5,
     PhotonFarForwardLooseTimingCut = 2,
     PhotonFarForwardTightTimingCut = 1,
     PhotonLooseTimingCut = 2,
     PhotonPtCut = 0,
     PhotonPtCutForLooseTiming = 4,
     PhotonTightTimingCut = 1,
     PtCutForTightTiming = 0.75,
     SelectedPfoCollection = 'SelectedPandoraPFOs',
     UseClusterLessPfos = 1,
     UseNeutronTiming = 0)

MyCLICPfoSelectorLoose = CLICPfoSelector \
    ('MyCLICPfoSelectorLoose',
     OutputLevel = WARNING,
     ChargedPfoLooseTimingCut = 3,
     ChargedPfoNegativeLooseTimingCut = -2.0,
     ChargedPfoNegativeTightTimingCut = -2.0,
     ChargedPfoPtCut = 0,
     ChargedPfoPtCutForLooseTiming = 4,
     ChargedPfoTightTimingCut = 1.5,
     CheckKaonCorrection = 0,
     CheckProtonCorrection = 0,
     ClusterLessPfoTrackTimeCut = 1000.,
     CorrectHitTimesForTimeOfFlight = 0,
     DisplayRejectedPfos = 1,
     DisplaySelectedPfos = 1,
     FarForwardCosTheta = 0.975,
     ForwardCosThetaForHighEnergyNeutralHadrons = 0.95,
     ForwardHighEnergyNeutralHadronsEnergy = 10,
     HCalBarrelLooseTimingCut = 20,
     HCalBarrelTightTimingCut = 10,
     HCalEndCapTimingFactor = 1,
     InputPfoCollection = 'PandoraPFOs',
     KeepKShorts = 1,
     MaxMomentumForClusterLessPfos = 2,
     MinECalHitsForTiming = 5,
     MinHCalEndCapHitsForTiming = 5,
     MinMomentumForClusterLessPfos = 0.0,
     MinPtForClusterLessPfos = 0.25,
     MinimumEnergyForNeutronTiming = 1,
     Monitoring = 0,
     MonitoringPfoEnergyToDisplay = 1,
     NeutralFarForwardLooseTimingCut = 2.5,
     NeutralFarForwardTightTimingCut = 1.5,
     NeutralHadronBarrelPtCutForLooseTiming = 3.5,
     NeutralHadronLooseTimingCut = 2.5,
     NeutralHadronPtCut = 0,
     NeutralHadronPtCutForLooseTiming = 8,
     NeutralHadronTightTimingCut = 1.5,
     PhotonFarForwardLooseTimingCut = 2,
     PhotonFarForwardTightTimingCut = 1,
     PhotonLooseTimingCut = 2.,
     PhotonPtCut = 0,
     PhotonPtCutForLooseTiming = 4,
     PhotonTightTimingCut = 2.,
     PtCutForTightTiming = 0.75,
     SelectedPfoCollection = 'LooseSelectedPandoraPFOs',
     UseClusterLessPfos = 1,
     UseNeutronTiming = 0)

MyCLICPfoSelectorTight = CLICPfoSelector \
    ('MyCLICPfoSelectorTight',
     OutputLevel = WARNING,
     ChargedPfoLooseTimingCut = 2.0,
     ChargedPfoNegativeLooseTimingCut = -0.5,
     ChargedPfoNegativeTightTimingCut = -0.25,
     ChargedPfoPtCut = 0,
     ChargedPfoPtCutForLooseTiming = 4,
     ChargedPfoTightTimingCut = 1.0,
     CheckKaonCorrection = 0,
     CheckProtonCorrection = 0,
     ClusterLessPfoTrackTimeCut = 10,
     CorrectHitTimesForTimeOfFlight = 0,
     DisplayRejectedPfos = 1,
     DisplaySelectedPfos = 1,
     FarForwardCosTheta = 0.95,
     ForwardCosThetaForHighEnergyNeutralHadrons = 0.95,
     ForwardHighEnergyNeutralHadronsEnergy = 10,
     HCalBarrelLooseTimingCut = 20,
     HCalBarrelTightTimingCut = 10,
     HCalEndCapTimingFactor = 1,
     InputPfoCollection = 'PandoraPFOs',
     KeepKShorts = 1,
     MaxMomentumForClusterLessPfos = 1.5,
     MinECalHitsForTiming = 5,
     MinHCalEndCapHitsForTiming = 5,
     MinMomentumForClusterLessPfos = 0.5,
     MinPtForClusterLessPfos = 1.0,
     MinimumEnergyForNeutronTiming = 1,
     Monitoring = 0,
     MonitoringPfoEnergyToDisplay = 1,
     NeutralFarForwardLooseTimingCut = 1.5,
     NeutralFarForwardTightTimingCut = 1,
     NeutralHadronBarrelPtCutForLooseTiming = 3.5,
     NeutralHadronLooseTimingCut = 2.5,
     NeutralHadronPtCut = 0.5,
     NeutralHadronPtCutForLooseTiming = 8,
     NeutralHadronTightTimingCut = 1.5,
     PhotonFarForwardLooseTimingCut = 2,
     PhotonFarForwardTightTimingCut = 1,
     PhotonLooseTimingCut = 2,
     PhotonPtCut = 0.2,
     PhotonPtCutForLooseTiming = 4,
     PhotonTightTimingCut = 1,
     PtCutForTightTiming = 1.0,
     SelectedPfoCollection = 'TightSelectedPandoraPFOs',
     UseClusterLessPfos = 0,
     UseNeutronTiming = 0)


def makePrimaryVertexFinder (name = 'PrimaryVertexFinder', **kw):
    from k4MarlinWrapper.MarlinProcessorWrapper import Args
    d = Args (name = name,
              BeamspotConstraint = 1,
              BeamspotSmearing = False,
              Chi2Threshold = 25.,
              TrackMaxD0 = 20.,
              TrackMaxInnermostHitRadius = 61,
              TrackMaxZ0 = 20.,
              TrackMinFtdHits = 999999,
              TrackMinTpcHits = 999999,
              TrackMinTpcHitsMinPt = 999999,
              TrackMinVtxFtdHits = 1,
              TrackMinVxdHits = 999999)
    d.update (kw)
    return d


def makeBuildUpVertex (name = 'BuildUpVertex', **kw):
    from k4MarlinWrapper.MarlinProcessorWrapper import Args
    d =  Args (name = name,
               AVFTemperature = 5.0,
               AssocIPTracks = 1,
               AssocIPTracksChi2RatioSecToPri = 2.0,
               AssocIPTracksMinDist = 0.,
               MassThreshold = 10.,
               MaxChi2ForDistOrder = 1.0,
               MinDistFromIP = 0.3,
               PrimaryChi2Threshold = 25.,
               SecondaryChi2Threshold = 9.,
               TrackMaxD0 = 10.,
               TrackMaxD0Err = 0.1,
               TrackMaxZ0 = 20.,
               TrackMaxZ0Err = 0.1,
               TrackMinFtdHits = 1,
               TrackMinPt = 0.1,
               TrackMinTpcHits = 1,
               TrackMinTpcHitsMinPt = 999999,
               TrackMinVxdFtdHits = 1,
               TrackMinVxdHits = 1,
               UseAVF = False,
               UseV0Selection = 1,
               V0VertexCollectionName = 'BuildUpVertices_V0')
    d.update (kw)
    return d


VertexFinder = makeLcfiplusProcessor \
    ('VertexFinder',
     Algorithms = [makePrimaryVertexFinder(), makeBuildUpVertex()],
     BeamSizeX = 38.2E-3,
     BeamSizeY = 68E-6,
     BeamSizeZ = 1.97,
     BuildUpVertexCollectionName = 'BuildUpVertices',
     PrimaryVertexCollectionName = 'PrimaryVertices')


VertexFinderUnconstrained = makeLcfiplusProcessor \
    ('VertexFinderUnconstrained',
     Algorithms = [makePrimaryVertexFinder (BeamspotConstraint = 0),
                   makeBuildUpVertex (V0VertexCollectionName = 'BuildUpVertices_V0_res')],
     BeamSizeX = 38.2E-3,
     BeamSizeY = 68E-6,
     BeamSizeZ = 1.97,
     BuildUpVertexCollectionName = 'BuildUpVertices_res',
     PFOCollection = 'TightSelectedPandoraPFOs',
     PrimaryVertexCollectionName = 'PrimaryVertices_res')


# Write output to EDM4hep
from Configurables import PodioOutput
out = PodioOutput("PodioOutput", filename = OutputFile)
out.outputCommands = ["keep *"]


algList.append(inp)
algList.append(MyAIDAProcessor)
algList.append(EventNumber)
algList.append(InitDD4hep)
algList.append(OverlayFalse)  # Config.OverlayFalse
# algList.append(Overlay91GeV)  # Config.Overlay91GeV
# algList.append(Overlay365GeV)  # Config.Overlay365GeV
algList.append(VXDBarrelDigitiser)
algList.append(VXDEndcapDigitiser)
algList.append(InnerPlanarDigiProcessor)
algList.append(InnerEndcapPlanarDigiProcessor)
algList.append(OuterPlanarDigiProcessor)
algList.append(OuterEndcapPlanarDigiProcessor)
# algList.append(MyTruthTrackFinder)  # Config.TrackingTruth
algList.append(MyConformalTracking)  # Config.TrackingConformal
algList.append(ClonesAndSplitTracksFinder)  # Config.TrackingConformal
algList.append(Refit)
algList.append(MyDDCaloDigi[CONSTANTS["CalorimeterIntegrationTimeWindow"]])
algList.append(MyDDSimpleMuonDigi)
algList.append(MyDDMarlinPandora)
algList.append(LumiCalReco)
algList.append(MyClicEfficiencyCalculator)
algList.append(MyRecoMCTruthLinker)
algList.append(MyTrackChecker)
algList.append(MyCLICPfoSelectorDefault)
algList.append(MyCLICPfoSelectorLoose)
algList.append(MyCLICPfoSelectorTight)
algList.append(RenameCollection)  # Config.OverlayFalse
# algList.append(MyFastJetProcessor)  # Config.OverlayNotFalse
algList.append(VertexFinder)
algList.append(JetClusteringAndRefiner)
# algList.append(VertexFinderUnconstrained)  # Config.VertexUnconstrainedON
algList.append(Output_REC)
algList.append(Output_DST)
algList.append(out)

from Configurables import ApplicationMgr
ApplicationMgr( TopAlg = algList,
                EvtSel = 'NONE',
                EvtMax   = EvtMax,
                ExtSvc = [evtsvc],
                OutputLevel=WARNING
              )
