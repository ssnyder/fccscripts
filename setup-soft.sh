#.  /cvmfs/sw.hsf.org/spackages7/key4hep-stack/2023-04-08/x86_64-centos7-gcc11.2.0-opt/urwcv/setup.sh
. /cvmfs/sw.hsf.org/key4hep/releases/2023-11-23/x86_64-centos7-gcc12.2.0-opt/key4hep-stack/2023-11-30-yi2hqy/setup.sh
#. /cvmfs/sw.hsf.org/key4hep/setup.sh
#. $FCC_ROOT/soft/key4hep-spack/scripts/cvmfs/setup-releases.sh 

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usatlas/u/snyder/MISSING-RPMS7/usr/lib64

function setroot() {
  found=false
  for p in  ${CMAKE_PREFIX_PATH//:/ }; do
    case $p in
      */$1/*) export $2=$p;
              found=true;
              break;;
    esac
  done
  if [ "$found" = "false" ]; then
    echo "WARNING: Cannot find package path for $1 ($2)"
  fi
}

setroot genfit   GENFIT_ROOT
setroot clhep    CLHEP_ROOT
setroot geant4   GEANT4_ROOT
setroot edm4hep  EDM4HEP_ROOT
setroot podio    PODIO_ROOT
setroot xerces-c XERCESC_ROOT
setroot zlib-ng  ZLIB_ROOT
setroot qt       QT_ROOT
#setroot vecgeom  VECGEOM_ROOT

export FCC_SOFT=$FCC_ROOT/soft
export FCC_SCRIPTS=$FCC_ROOT/fccScripts
export EDITOR=emacsclient
export VISUAL=emacsclient
export FCC_INST=$FCC_SOFT/inst
export FCC_BUILD=$FCC_SOFT/build
export PATH=$FCC_INST/bin:$PATH
export LD_LIBRARY_PATH=$FCC_INST/lib:$FCC_INST/lib64:$LD_LIBRARY_PATH
export PYTHONPATH=$FCC_INST/python:$FCC_INST/lib/python3.10/site-packages:$PYTHONPATH
export CMAKE_PREFIX_PATH=$FCC_INST:$CMAKE_PREFIX_PATH
export DD4hepINSTALL=$FCC_INST
export DD4HEP=$FCC_SOFT/DD4hep

export MARLIN_DLL=`$FCC_SCRIPTS/updateMarlinDll.py $FCC_INST/lib $FCC_INST/lib64`

export ROMESYS=$FCC_BUILD/rome3

export ROOT_INCLUDE_PATH=$FCC_INST/include:$ROOT_INCLUDE_PATH
