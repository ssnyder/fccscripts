#!/usr/bin/env python

import os
import sys

libdirs = sys.argv[1:]

out = []
for lib in os.environ['MARLIN_DLL'].split(':'):
    if not os.path.exists (lib):
        out.append (lib)
        continue
    bn = os.path.basename(lib)
    for libdir in libdirs:
        p = os.path.join (libdir, bn)
        if os.path.exists (p):
            out.append (p)
            break
    else:
        out.append (lib)

print (':'.join (out))
