#!/usr/bin/env python

# Print out versions of packages set up by the current key4hep setup.

import os

packs = {}

def checkpath(p):
    if p.startswith ('/cvmfs/sw.hsf.org/spackages7/'):
        pp = p.split ('/')
        packs[pp[4]] = pp[5]
    return


def printpacks (packs):
    for k in sorted (packs.keys()):
        print (f'{k:25s} {packs[k]}')
    return

for p in os.environ['LD_LIBRARY_PATH'].split(':'):
    checkpath(p)
printpacks (packs)

