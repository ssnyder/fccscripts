
model = SM
alias n = n1:n2:n3
alias N = N1:N2:N3
alias q = u:d:s:c:b
alias Q = U:D:S:C:B
alias l = e1:e2:e3
alias L = E1:E2:E3

sqrts = 240 GeV
seed = 202300

?resonance_history = true
process zz_mmnn_18000 = "e+", "e-" => (e2, E2, n, N) { $restrictions = "3+4 ~ Z && 5+6 ~ Z" }
compile

cuts = all abs(M-mZ) < 5 GeV [e2,E2]
   and all abs(M-mZ) < 5 GeV [n,N]


$title = "mmm"
histogram mmm (60 GeV, 120 GeV) { n_bins = 50 }
$title = "mnn"
histogram mnn (60 GeV, 120 GeV) { n_bins = 50 }
analysis =
  let @gam = select if PDG==22 [@evt] in
  let @isrgam = extract index 1 [@gam] & extract index 2 [@gam] in
  let @fsrgam = select if Theta > 0.01 [A, @isrgam] in
  let @lepgam = [@fsrgam] & [e2:E2] in
  record mmm (eval M [collect [@lepgam]]);
  record mnn (eval M [n,N])


beams = "e+", "e-" => gaussian => isr
?keep_beams  = true    
?keep_remnants = true
beams_momentum = 120 GeV, 120 GeV
gaussian_spread1 = 0.185%
gaussian_spread2 = 0.185%

?isr_handler       = true
$isr_handler_mode = "recoil"
isr_alpha          = 0.0072993
isr_mass           = 0.000511



! Parton shower and hadronization
?ps_fsr_active          = true
?ps_isr_active          = false
?hadronization_active   = true
$shower_method          = "PYTHIA6"
!?ps_PYTHIA_verbose     = true


#OPAL tune  cf https://agenda.linearcollider.org/event/4917/contributions/20431/attachments/16642/27089/pythiatune-ilc.pdf
# mstj(11)=3            --- longitudinal fragmentation scheme (hybrid)
# mstp(3)=1             --- lambda meaning
# parj(1)=0.08500       --- qq/q
# parj(2)=0.31000       --- s/u
# parj(3)=0.45000       --- su/du
# parj(4)=0.02500       --- S=1/S=2 diquark suppr.
# parj(11)=0.60000      --- S=1 (d,u)
# parj(12)=0.40000      --- S=1 (s)
# parj(13)=0.72000      --- S=1 (c,b)
# parj(14)=0.43000      --- S=1,s=0 prob
# parj(15)=0.08000      --- S=0,s=1 prob
# parj(16)=0.08000      --- S=1,s=1 prob
# parj(17)=0.17000      --- tensor mesons (L=1)
# parj(21)=0.40000      --- primary hadron transverse mom width
# parj(41)=0.11000      --- lund fragmentation function a parameter
# parj(42)=0.52000      --- lund fragmentation function b parameter
# parj(54)=-0.03100     --- eps_charm
# parj(55)=-0.00200     --- eps_bottom
# parj(81)=0.25000      ---  parton shower lambda
# parj(82)=1.90000      ---  qcd shower q0 cutoff in fsr

# parj(19) and parj(26) disabled in opal tune, but
# not listed explicitly below?

# Other parameters
# mstj(22)=4            --- decay length cutoff parj(73..74)
# parj(73)=2250         ---  transverse distance cutoff
# parj(74)=2500         ---  z distance cutoff
# mstj(28)=0            --- decau taus internally.
# mstj(41)=2            --- allow q->qgam and e->egam in FSR
# pmas(25,1)=125.       --- h mass
# pmas(25,2)=0.4143E-02 --- h width
# mstu(22)=2000         --- max number of errors to print
# mstp(71)=1            --- enable final state qcd/qed (this is the default value)
# mstp(151)=1           --- smear primary vertex
# parp(151)=0.0098      ---  sigma_x
# parp(152)=2.54e-5     ---  sigma_y
# PARP(153)=0.646       ---  sigma_z
# parp(154)=1.937       ---  sigma_t

# nb. turn off vertex smearing for now.
#   if it's on and the H is decayed by whizard (rather than pythia),
#   then we can get errors from ddsim when reading the event:
#     Input            FATAL +++ Stable particle (PDG: 11        ) with daughters! check your MC record, adapt particle.tbl file...
#    when the H decay products get a positive time.
# Do the smearing instead in ddsim.
$ps_PYTHIA_PYGIVE = "MSTJ(28)=0; PMAS(25,1)=125.; PMAS(25,2)=0.4143E-02; MSTJ(41)=2; MSTU(22)=2000; PARJ(21)=0.40000; PARJ(41)=0.11000; PARJ(42)=0.52000; PARJ(81)=0.25000; PARJ(82)=1.90000; MSTJ(11)=3; PARJ(54)=-0.03100; PARJ(55)=-0.00200; PARJ(1)=0.08500; PARJ(3)=0.45000; PARJ(4)=0.02500; PARJ(2)=0.31000; PARJ(11)=0.60000; PARJ(12)=0.40000; PARJ(13)=0.72000; PARJ(14)=0.43000; PARJ(15)=0.08000; PARJ(16)=0.08000; PARJ(17)=0.17000; MSTP(3)=1;MSTP(71)=1;  MSTP(151)=0; PARP(151)=0.0098; PARP(152)=2.54e-5; PARP(153)=0.646; PARP(154)=1.937; MSTJ(22)=4; PARJ(73)=2250; PARJ(74)=2500"

integrate (zz_mmnn_18000)
simulate (zz_mmnn_18000) {
  n_events = 18000
  $sample = "zz_mmnn.2023_18000"
  sample_format = stdhep
  $extension_stdhep = "stdhep"
  !sample_format = ascii
}
compile_analysis { $out_file = "zz_mmnn_ana.dat" }
